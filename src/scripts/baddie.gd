extends KinematicBody2D

export var speed = 50
export var gravity = 500
export var direction = 1
onready var sprite = $sprite
var active = false

var velocity = Vector2.ZERO
export (float, 0, 1.0) var acceleration = 0.25
export (float, 0, 1.0) var friction = 0.1
var paused = false
var dead = false
onready var hit_area = $hit_area/CollisionShape2D

onready var collision = $CollisionShape2D
onready var snd_death = $snd_death

func _ready():
	add_to_group("baddies")
	add_to_group("teleportable")
	sprite.play("walk")
	if $VisibilityNotifier2D.is_on_screen():
		active = true
	
func _physics_process(delta):
	if dead:
		dead()
		
	if !paused && active:
		walk(delta)
		velocity.y += gravity * delta
		velocity = move_and_slide(velocity, Vector2.UP)
	
func walk(delta):
	if is_on_wall():
		direction *= -1
		
	if direction == 1:
		sprite.flip_h = 0
	else:
		sprite.flip_h = 1
		
	if direction != 0:
		velocity.x = lerp(velocity.x, direction * speed * delta * 50, acceleration)
	else:
		velocity.x = lerp(velocity.x, 0, friction)


func attack(body):
	body.take_damage(1)

func dead():
	collision.disabled = true
	rotation += .1

func death():
	dead = true
	snd_death.play()
	hit_area.disabled = true
	collision.disabled = true
	gravity = 500
	modulate.a = 0.5
	velocity = Vector2(0,-100)
	yield(get_tree().create_timer(2.0), "timeout")
	queue_free()
	

func _on_VisibilityNotifier2D_viewport_entered(viewport):
	active = true
