extends KinematicBody2D

#Godot Basic Platform Player Script
#Copyright (C) 2021  Kristofer Occhipinti - Films By Kris
#http://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

export (int) var speed = 500
export (int) var jump_speed = -500
export (int) var gravity = 800

export (float, 0, 1.0) var friction = 0.1
export (float, 0, 1.0) var acceleration = 0.25

export (float, 0, 1.0) var hue = 0.0

var velocity = Vector2.ZERO
onready var sprite = $Sprite
onready var collision = $CollisionShape2D
onready var camera = $Camera2D
onready var start_pos = position

onready var snd_jump = $snd_jump
onready var snd_death = $snd_death

var invincible = false
var paused = false
var dead = false

func _ready():
	add_to_group("players")
	add_to_group("teleportable")
	hue_shift(hue)
	
func _physics_process(delta):
	if !dead:
		get_input(delta)
		stomp()
	else:
		dead()
	
	animation()
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)

	
func get_input(delta):
	if !paused:
		walk(delta)
		jump(delta)
	else:
		velocity = Vector2(0,0)

func stomp():
	for index in range(get_slide_count()):
		# We check every collision that occurred this frame.
		var collision = get_slide_collision(index)
		
		# If we collide with a monster...
		if collision.collider.is_in_group("baddies"):
			var baddie = collision.collider
			# ...we check that we are hitting it from above.
			if Vector2.UP.dot(collision.normal) > 0.1:
				# If so, we squash it and bounce.
				baddie.death()
				velocity.y = -250

func hue_shift(hue):
	# Duplicate the shader so that changing its param doesn't change it on any other sprites that also use the shader.
	# Generally done once in _ready()
	sprite.set_material(sprite.get_material().duplicate(true))

	# Offset sprite hue by a random value within specified limits.
	#var rand_hue = float(randi() % 3)/2.0/3.2
	sprite.material.set_shader_param("Shift_Hue", hue)


func walk(delta):
	var dir = 0
	if Input.is_action_pressed("walk_right"):
		dir += 1
		sprite.flip_h = 0
	if Input.is_action_pressed("walk_left"):
		dir -= 1
		sprite.flip_h = 1
	if dir != 0:
		velocity.x = lerp(velocity.x, dir * speed * delta * 50, acceleration)
	else:
		velocity.x = lerp(velocity.x, 0, friction)
	
func jump(delta):
	if Input.is_action_just_pressed("jump"):
		if is_on_floor():
			snd_jump.play()
			velocity.y = jump_speed
		
func animation():
	if velocity.x > 15 || velocity.x < -15:
		sprite.play("walk")
	else:
		sprite.play("stand")

func take_damage(damage):
	if !invincible:
		death()
		
func death():
	snd_death.play()
	camera.current = false
	modulate.a = 0.5
	dead = true
	invincible = true
	velocity = Vector2(0,-500)
	yield(get_tree().create_timer(2.0), "timeout")
	respawn()
	
func respawn():
	velocity = Vector2()
	modulate.a = 0.5
	collision.disabled = false
	var pos = start_pos
	for p in get_tree().get_nodes_in_group("players"):
		if p != self && !p.dead:
			pos = p.position
			break
	
	
	dead = false
	position = pos
	rotation = 0
	
	for p in get_tree().get_nodes_in_group("players"):
		if p.camera.current:
			break
		else:
			camera.current = true
	
	
	yield(get_tree().create_timer(1.0), "timeout")
	modulate.a = 1
	invincible = false
	

func dead():
	collision.disabled = true
	rotation += .1
	
