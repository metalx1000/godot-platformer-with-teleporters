extends Area2D

export var id = 0

func _on_Teleporter_body_entered(body):
	if body.is_in_group("teleportable"):
		var landings = get_tree().get_nodes_in_group("teleporter_landings")
		for landing in landings:
			if landing.id == id:
				landing.sound.play()
				landing.visible = true

				body.paused = true
				var xpos = landing.position.x - body.scale.x/2
				var ypos = landing.position.y
				body.position = Vector2(xpos,ypos)
				yield(get_tree().create_timer(.5), "timeout")
				body.paused = false

				landing.visible = false
