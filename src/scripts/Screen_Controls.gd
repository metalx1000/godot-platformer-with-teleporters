extends CanvasLayer

func _ready():
	pass # Replace with function body.


func hide_controls():
		
	for btn in get_children():
		if btn.visible:
			btn.visible = false
		else:
			btn.visible = true
			
	$hide.visible = true
