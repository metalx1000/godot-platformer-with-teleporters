extends "res://src/scripts/baddie.gd"

#set gravity to '0' for streight flying

onready var timer = $Gravity_Timer
export (float) var updown_time = 1.0

func _ready():
	timer.wait_time = updown_time
	
func _on_Gravity_Timer_timeout():
	if !dead:
		gravity *= -1
